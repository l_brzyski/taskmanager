# Task Manager


## Background

A simple web app for distributing tasks between managers and subordinates. A purpose of this app - to play around with Dropwizard :)

#### Why?
**As a** *Manager*  
**I want to** create and delegate tasks to my subordinates  
**So that** they know what needs to be done.

**As a** *Manager*  
**I want** to know what tasks are assigned to my subordinates  
**So that** I can see who is working on what

**As a** *Subordinate*  
**I want** to see what tasks are assigned to me  
**So that** I know what needs to be done

## Follow up
This small all application will be delivered in small MVPs.  
#### Business requirements
##### MVP 1  
**As a** *SuperUser*  
**I want** to be able to log in to the system with username "admin" and password "s3cret"  
**So that** I can access system settings page.

##### MVP 2
**As a** *SuperUser*  
**I want** to add users with Manager and Subordinates roles with username and password  
**So that** they can log in into the system with set up credentials

##### MPV 3
**As a** *Manager*
**I want** to add a task into the system and assign it to one of subordinates  
**So that** task is stored in the system

##### MVP 4
**As a** *Subordinate*  
**I want** to see assigned to me tasks

##### MVP 5
**As a** *Manager*  
**I want** to list all the tasks in the system  
**So that** I know what tasks are assigned to subordinates in the system 

#### Technical requirements
- DropWizard framework
- unit testing

#### UX requirements
##### MVP 1
Plain HTML (no styling)

##### MVP 2
Bootstrap

#### Acceptance criteria
see Business requirements - user stories


## Proposed solution
##### MVP 1
Very simplistic MVP, skeleton of the Dropwizard app with simple authentication (no authorization ath this point). No Database. Username and password for *Superuser* will be stored in project settings.

##### MVP 2
Adding database to the project (local H2). Adding real *User* to the system, with authentication (*Superuser*,*Manager*,*Subordinate*). *Superuser* predefined in the system, inserted into the database during first run of the system.

##### MVP 3
Definition of *Task* in the system (database). A simple webpage to add task (title, text, assignee - *Subordinate*).

##### MVP 4
A new webpage listing tasks for logged in *Subordinate*

##### MVP 5
A new webpage listing tasks for all *Subordinates*. Visible only after logging in into the system by manager.

## Decision log