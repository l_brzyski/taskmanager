package com.lukaszbrzyski.taskmanager;

/**
 * Created by lukasz on 15/01/15.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class TaskManagerConfiguration extends Configuration {

	private String defaultName = "NoName?";

	private String superuserPassword;

	@Valid
	@NotNull
	private DataSourceFactory database = new DataSourceFactory();


	@JsonProperty
	public String getDefaultName() {
		return defaultName;
	}

	@JsonProperty
	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	@JsonProperty
	public String getSuperuserPassword() {
		return superuserPassword;
	}

	@JsonProperty
	public void setSuperuserPassword(String superuserPassword) {
		this.superuserPassword = superuserPassword;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory() {
		return database;
	}

	@JsonProperty("database")
	public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
		this.database = dataSourceFactory;
	}
}
