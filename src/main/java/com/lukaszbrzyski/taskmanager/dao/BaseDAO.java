package com.lukaszbrzyski.taskmanager.dao;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import io.dropwizard.jersey.params.LongParam;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BaseDAO<T> extends AbstractDAO<T> {

	private static final Logger logger = LoggerFactory.getLogger(BaseDAO.class);

	private Class<T> dataTypeClass;

	public BaseDAO(Class<T> dataTypeClass, SessionFactory factory) {
		super(factory);
		this.dataTypeClass = dataTypeClass;
	}

	public Optional<T> findById(LongParam id) {
		logger.trace("findById:  : " + id);
		return Optional.fromNullable(get(id.get()));
	}

	public Optional<T> findByName(String name) {
		logger.trace("findByName: " + name);
		return Optional.fromNullable(uniqueResult(namedQuery(dataTypeClass.getCanonicalName() + ".findByName").setString("name", name)));
	}

	public List<T> findAll() {
		logger.trace("findAll: " + dataTypeClass.getCanonicalName());
		return list(namedQuery(dataTypeClass.getCanonicalName() + ".findAll"));
	}

	public T save(T entity) {
		logger.trace("save  : " + entity);
		return persist(entity);
	}

}