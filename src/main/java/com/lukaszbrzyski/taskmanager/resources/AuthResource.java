package com.lukaszbrzyski.taskmanager.resources;

import com.google.common.base.Optional;
import com.lukaszbrzyski.taskmanager.dao.BaseDAO;
import com.lukaszbrzyski.taskmanager.model.Role;
import com.lukaszbrzyski.taskmanager.model.User;
import com.lukaszbrzyski.taskmanager.views.AdminView;
import com.lukaszbrzyski.taskmanager.views.AuthorizedView;
import com.lukaszbrzyski.taskmanager.views.ManagerView;
import com.lukaszbrzyski.taskmanager.views.SubordinateView;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.security.AccessControlException;
import java.util.List;

@Path("/")
public class AuthResource {
	private static final Logger logger = LoggerFactory.getLogger(AuthResource.class);

	public static final String ROLE_ADMIN = "admin";
	public static final String ROLE_MANAGER = "manager";
	public static final String ROLE_SUBORDINATE = "subordinate";

	private final BaseDAO<Role> roleDAO;

	public AuthResource(BaseDAO<Role> roleDAO) {
		this.roleDAO = roleDAO;
	}

	@GET
	@Produces(MediaType.TEXT_HTML)
	@UnitOfWork
	public AuthorizedView getAuthorizedView(@Auth User user) {

		Optional<Role> adminRole = roleDAO.findByName(ROLE_ADMIN);
		Optional<Role> managerRole = roleDAO.findByName(ROLE_MANAGER);
		Optional<Role> subordinateRole = roleDAO.findByName(ROLE_SUBORDINATE);

		if (adminRole.isPresent() && user.getRoles().contains(adminRole.get())) {
			List<Role> roles = roleDAO.findAll();
			logger.trace("opening admin page");
			return new AdminView(roles, user.getName());
		} else if (managerRole.isPresent() && user.getRoles().contains(managerRole.get())) {
			logger.trace("opening manager page");
			return new ManagerView(user.getName());
		}  else if (subordinateRole.isPresent() && user.getRoles().contains(subordinateRole.get())) {
			logger.trace("opening manager page");
			return new SubordinateView(user.getName());
		}

	   throw new AccessControlException("No Roles applicable for user " + user.getName());
	}
}
