package com.lukaszbrzyski.taskmanager.resources;

import com.google.common.base.Optional;
import com.lukaszbrzyski.taskmanager.dao.BaseDAO;
import com.lukaszbrzyski.taskmanager.model.User;
import com.sun.jersey.api.NotFoundException;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/v1/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	private final BaseDAO<User> userDAO;

	public UserResource(BaseDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

	@GET
	@Path("/{userId}")
	@UnitOfWork
	public User getUserById(@PathParam("userId") LongParam userId) {
		final Optional<User> user = userDAO.findById(userId);
		if (!user.isPresent()) {
			throw new NotFoundException("No user with id " + userId);
		}
		return user.get();
	}

	@GET
	@UnitOfWork
	public List<User> getAllUsers() {
		final List<User> users = userDAO.findAll();
		return users;
	}

	@POST
	@UnitOfWork
	public User addUser(@Valid User newUser) {
		return userDAO.save(newUser);
	}

}
