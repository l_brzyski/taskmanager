package com.lukaszbrzyski.taskmanager.views;

import io.dropwizard.views.View;

public abstract class AuthorizedView extends View{

	private final String username;

	public AuthorizedView(String username, String freemarkerTemplate) {
		super(freemarkerTemplate);
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

}
