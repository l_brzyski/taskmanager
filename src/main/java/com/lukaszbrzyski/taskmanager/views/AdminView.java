package com.lukaszbrzyski.taskmanager.views;

import com.lukaszbrzyski.taskmanager.model.Role;

import java.util.List;

public class AdminView extends AuthorizedView{

	private static String freemarkerTemplate = "admin.ftl";
	private List<Role> roles;

	public AdminView(List<Role> roles, String username) {
		super(username, freemarkerTemplate);
		this.roles = roles;
	}

	public List<Role> getRoles() {
		return roles;
	}
}
