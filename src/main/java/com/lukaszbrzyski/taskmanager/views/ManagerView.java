package com.lukaszbrzyski.taskmanager.views;

public class ManagerView extends AuthorizedView{

	private static String freemarkerTemplate = "manager.ftl";

	public ManagerView(String username) {
		super(username, freemarkerTemplate);
	}
}
