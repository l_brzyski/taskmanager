package com.lukaszbrzyski.taskmanager.views;

public class SubordinateView extends AuthorizedView{

	private static String freemarkerTemplate = "subordinate.ftl";

	public SubordinateView(String username) {
		super(username, freemarkerTemplate);
	}
}
