package com.lukaszbrzyski.taskmanager;

/**
 * Created by lukasz on 15/01/15.
 */

import com.lukaszbrzyski.taskmanager.auth.PasswordAuthenticator;
import com.lukaszbrzyski.taskmanager.dao.BaseDAO;
import com.lukaszbrzyski.taskmanager.model.Role;
import com.lukaszbrzyski.taskmanager.model.User;
import com.lukaszbrzyski.taskmanager.resources.AuthResource;
import com.lukaszbrzyski.taskmanager.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.basic.BasicAuthProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskManagerApplication extends Application<TaskManagerConfiguration> {

	private static final Logger logger = LoggerFactory.getLogger(TaskManagerApplication.class);

	public static void main(String[] args) throws Exception {
		new TaskManagerApplication().run(args);
	}

	private final HibernateBundle<TaskManagerConfiguration> hibernateBundle =
			new HibernateBundle<TaskManagerConfiguration>(User.class, Role.class) {
				@Override
				public DataSourceFactory getDataSourceFactory(TaskManagerConfiguration configuration) {
					return configuration.getDataSourceFactory();
				}
			};


	@Override
	public String getName() {
		return "taskmanager";
	}

	@Override
	public void initialize(Bootstrap<TaskManagerConfiguration> bootstrap) {
		bootstrap.addBundle(new AssetsBundle("/public", "/public"));
		bootstrap.addBundle(new ViewBundle());


		bootstrap.addBundle(new MigrationsBundle<TaskManagerConfiguration>() {
			@Override
			public DataSourceFactory getDataSourceFactory(TaskManagerConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}
		});
		bootstrap.addBundle(hibernateBundle);
	}

	@Override
	public void run(TaskManagerConfiguration configuration, Environment environment) {

		final BaseDAO<Role> roleDAO = new BaseDAO(Role.class, hibernateBundle.getSessionFactory());
		final BaseDAO<User> userDAO = new BaseDAO(User.class, hibernateBundle.getSessionFactory());

		environment.jersey().register(new AuthResource(roleDAO));
		environment.jersey().register(new UserResource(userDAO));

		environment.jersey().register(new BasicAuthProvider<>(new PasswordAuthenticator(userDAO),
				"secure area. By default type admin/admin123"));


	}
}
