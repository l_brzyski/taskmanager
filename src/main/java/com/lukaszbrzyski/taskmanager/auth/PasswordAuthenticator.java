package com.lukaszbrzyski.taskmanager.auth;

import com.google.common.base.Optional;
import com.lukaszbrzyski.taskmanager.dao.BaseDAO;
import com.lukaszbrzyski.taskmanager.model.User;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class PasswordAuthenticator implements Authenticator<BasicCredentials, User> {

	private final BaseDAO<User> userDAO;

	public PasswordAuthenticator(BaseDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

	@Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
		Optional<User> user = userDAO.findByName(credentials.getUsername());
        if (user.isPresent() && user.get().getPassword().equals(credentials.getPassword())) {
            return user;
        }
        return Optional.absent();
    }
}
