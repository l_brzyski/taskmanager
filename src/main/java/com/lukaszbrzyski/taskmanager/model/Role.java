package com.lukaszbrzyski.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@NamedQueries({
	@NamedQuery(
		name = "com.lukaszbrzyski.taskmanager.model.Role.findAll",
		query = "SELECT r FROM Role r"
	),
	@NamedQuery(
		name = "com.lukaszbrzyski.taskmanager.model.Role.findByName",
		query = "SELECT r FROM Role r WHERE r.name = :name"
	)
})
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", nullable = false)
	private String name;


	public Role() {
	}

	public Role(String name) {
		this.name = name;
	}


	@JsonProperty
	public long getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Role role = (Role) o;

		if (id != role.id) return false;
		if (!name.equals(role.name)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + name.hashCode();
		return result;
	}
}
