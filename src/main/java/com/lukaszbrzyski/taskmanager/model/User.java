package com.lukaszbrzyski.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
@NamedQueries({
	@NamedQuery(
		name = "com.lukaszbrzyski.taskmanager.model.User.findAll",
		query = "SELECT u FROM User u"
	),
	@NamedQuery(
		name = "com.lukaszbrzyski.taskmanager.model.User.findByName",
		query = "SELECT u FROM User u WHERE u.name = :name"
	)
})
public class User {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", nullable = false)
	@NotEmpty(message = "username can not be empty")
	private String name;

	@Column(name = "password", nullable = false)
	@NotEmpty(message = "Password can not be empty")
	private String password;

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	@JoinTable(name="users_roles",
			joinColumns={@JoinColumn(name="users_id")},
			inverseJoinColumns={@JoinColumn(name="roles_id")})
	private Set<Role> roles;

	public User() {
	}

	public User(String name, String password) {
		this.name = name;this.password = password;
	}

	public User(String name, String password, Set<Role> roles) {
		this.name = name;
		this.password = password;
		this.roles = roles;
	}

	@JsonProperty
	public long getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
        return name;
    }

	@JsonProperty
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public Set<Role> getRoles() {
		return roles;
	}



	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (id != user.id) return false;
		if (!name.equals(user.name)) return false;
		if (!password.equals(user.password)) return false;
		if (!roles.equals(user.roles)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + name.hashCode();
		result = 31 * result + password.hashCode();
		result = 31 * result + roles.hashCode();
		return result;
	}
}
