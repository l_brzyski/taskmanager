<#-- @ftlvariable name="" type="com.lukaszbrzyski.taskmanager.views.AdminView" -->
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Task Managment System</title>

    <!-- Bootstrap -->
    <link href="public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container theme-showcase">
    <div class="jumbotron">
        <h1>Task Managment system</h1>

        <h3>Admin console</h3>

        <div>Logged as : ${username}</div>
    </div>

    <div>
        <div class="page-header">
            <h1>Add new system user</h1>
        </div>
        <div>
            <table>
                <tbody>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" id="name" name="name"/></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" id="password" name="password"/></td>
                </tr>
                <tr>
                    <td>Roles:</td>
                    <td>
                        <div id="roles">
                            <#list roles as role>
                                <input type="checkbox" name="${role.name}" id="${role.id}"/> ${role.name}<br>
                            </#list>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            <button class="btn btn-xs btn-success" id="addUser">Add User</button>

            <br>

            <div id="failure_alert" hidden="true" class="alert alert-danger" role="alert"/>
            <div id="success_alert" hidden="true" class="alert alert-success" role="alert"/>
        </div>
    </div>
    <div class="page-header">
        <h1>Existing system users</h1>
    </div>
    <table id="users_table" class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Roles</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <script type='text/javascript' src='/public/jquery.js'></script>
    <script>
        $(function () {
            $.getJSON("/v1/users", function (users) {
                for (var i = 0; i < users.length; i++) {
                    $("#users_table tr:last").after("<tr><td>" + (i + 1) + "</td><td>" + users[i].name + "</td><td>" + display_role_list(users[i].roles) + "</td></tr>")
                }
            });

            function display_role_list(roles) {
                var role_list = "";
                for (var j = 0; j < roles.length; j++) {
                    role_list += "<span class=\"label label-primary\">" + roles[j].name + "</span> &nbsp;"
                }
                return role_list;
            }

            $("#addUser").click(function () {
                var data = {};
                data[$("#name").attr("name")] = $("#name").val();
                data[$("#password").attr("name")] = $("#password").val();
                data["roles"] = []

                $('#roles input:checked').each(function () {
                    var role = {};
                    role.name = $(this).attr('name');
                    role.id = $(this).attr('id');
                    data["roles"].push(role);
                });

                $.ajax({
                    url: "/v1/users",
                    type: "POST",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        $("#users_table tr:last").after("<tr><td>" + $('#users_table tr').length + "</td><td>" + data.name + "</td><td>" + display_role_list(data.roles) + "</td></tr>")
                        $("#failure_alert").hide();
                        $("#success_alert").text("User " + data.name + " added");
                        $("#success_alert").show();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#success_alert").hide();
                        $("#failure_alert").text("An error occured. User not added.");
                        $("#failure_alert").show();
                    }
                })
            })
        })
    </script>

</div>
</body>
</html>