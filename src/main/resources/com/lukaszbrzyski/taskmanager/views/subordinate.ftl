<#-- @ftlvariable name="" type="com.lukaszbrzyski.taskmanager.views.ManagerView" -->
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Task Managment System</title>

    <!-- Bootstrap -->
    <link href="public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container theme-showcase">
    <div class="jumbotron">
        <h1>Task Managment system</h1>

        <h3>Subordinate console</h3>

        <div>Logged as : ${username}</div>
    </div>

    <div>
        <div class="page-header">
            <h1>Browse my tasks</h1>
        </div>
        <div>...At some point you will be able to browse through your tasks.</div>
    </div>
</body>
</html>