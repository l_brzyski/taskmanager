package com.lukaszbrzyski.taskmanager.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Optional;
import com.lukaszbrzyski.taskmanager.dao.BaseDAO;
import com.lukaszbrzyski.taskmanager.model.Role;
import com.lukaszbrzyski.taskmanager.model.User;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.eclipse.jetty.server.Response;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
/**
 * Unit tests for {@link UserResource}.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserResourceTest {
	private static final BaseDAO<User> dao = mock(BaseDAO.class);

	@ClassRule
	public static final ResourceTestRule resources = ResourceTestRule.builder()
			.addResource(new UserResource(dao))
			.build();
	@Captor
	private ArgumentCaptor<User> userCaptor;

	private User user1;
	private User user2;
	private List<User> users;

	@Before
	public void setUp() {
		Set<Role> roles = new HashSet();
		roles.add(new Role("admin"));
		user1 = new User("admin", "p@zzw0rd", roles);
		user2 = new User("manager", "s3cret", roles);

		users = new ArrayList<>();
		users.add(user1);
		users.add(user2);
	}

	@After
	public void tearDown() {
		reset(dao);
	}

	@Test
	public void saveUser() throws JsonProcessingException {
		when(dao.save(any(User.class))).thenReturn(user1);
		ClientResponse response = resources.client().resource("/v1/users")
				.type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, user1);

		assertThat("Response status is 200 OK", response.getStatus(), equalTo(Response.SC_OK));
		User result = response.getEntity(User.class);
		assertTrue(result.equals(user1));

		verify(dao).save(userCaptor.capture());
		assertTrue(userCaptor.getValue().equals(user1));
	}

	@Test
	public void getUserById() throws JsonProcessingException {
		when(dao.findById(any(LongParam.class))).thenReturn(Optional.fromNullable(user1));
		User response = resources.client().resource("/v1/users/1")
				.type(MediaType.APPLICATION_JSON)
				.get(User.class);

		assertTrue(response.equals(user1));
		verify(dao).findById(any(LongParam.class));

	}

	@Test
	public void getAllUsers() throws JsonProcessingException {
		when(dao.findAll()).thenReturn(users);

		List<User> response = resources.client().resource("/v1/users")
				.type(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<User>>(){
				});

		assertTrue(response.size() == 2);
		assertTrue(response.equals(users));

		verify(dao).findAll();
		assertTrue(response.equals(users));
	}

}
